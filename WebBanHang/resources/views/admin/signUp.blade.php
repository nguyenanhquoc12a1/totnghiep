@extends('layoutLoginAdmin')
@section('contentlogin')      
<div class="card z-index-0 fadeIn3 fadeInBottom">
              <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-primary shadow-primary border-radius-lg py-3 pe-1">
                  <h4 class="text-white font-weight-bolder text-center mt-2 mb-0">Sign Up</h4>
                </div>
              </div>
              <div class="card-body">
                <form role="form" action="{{URL::to('/')}}" class="text-start">
                  <div class="input-group input-group-outline my-3">
                    <input type="email" name="email" class="form-control" placeholder="Email">
                  </div>
                  <div class="input-group input-group-outline my-3">
                    <input type="text" name="name" class="form-control" placeholder="Name">
                  </div>
                  <div class="input-group input-group-outline mb-3">
                    <input type="text" name="address" class="form-control" placeholder="Address">
                  </div>
                  <div class="input-group input-group-outline my-3">
                    <input type="number" name="phone" class="form-control" placeholder="Phone">
                  </div>
                  <div class="input-group input-group-outline my-3">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                  </div>
                  <div class="input-group input-group-outline my-3">
                    <input type="password" name="confirmpassword" class="form-control" placeholder="Confirm password">
                  </div>
                  <div class="text-center">
                    <button type="button" class="btn bg-gradient-primary w-100 my-4 mb-2">Sign Up</button>
                  </div>
                  <div class="card-footer text-center pt-0 px-lg-2 px-1">
                  <p class="mb-2 text-sm mx-auto">
                    Already have an account?
                    <a href="{{URL::to('login')}}" class="text-primary text-gradient font-weight-bold">Sign in</a>
                  </p>
                </div>
                </form>
              </div>
            </div>
@endsection