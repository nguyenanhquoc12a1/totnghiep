<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index(){
        return view('layoutAdmin');
    }

    // public function dashboard(){
    //     return view('layoutAdmin');
    // }
    
    public function login(){
        return view('admin.signIn');
    }

    public function register(){
        return view('admin.signUp');
    }

    public function product(){
        return view('admin.product');
    }

    public function addproduct(){
        return view('admin.addproduct');
    }
    
}
